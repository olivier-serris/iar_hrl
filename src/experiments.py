from DBN.node import *
from DBN.agent import Agent
import gym
import time
import json
import datetime
import os
from DBN.dbn import DBN,get_refinements_validity_count
from envs.coffee_task import CoffeeTask
import matplotlib.pyplot as plt 

ENV_INFOS = {'Taxi-v3' : 
                        {'n_actions' : 6,
                        'n_state_var' : 4,
                        'state_var_dom' : 5,
                        'encoded' : True , 
                         },

            'CoffeeTask' : {'n_actions' : 4,
                        'n_state_var' : 6,
                        'state_var_dom' : 2,
                        'encoded' : False, 
                        'real_refinements_nb' : 7,}}

def make_str(params):
    env_name = params['env'] if isinstance(params['env'],str) else params['env'].__name__ 
    return f'env_name_{env_name}_'+ '_'.join([f'{k}={v}' 
                                            for k,v in params.items() if k not in ['env','nb_steps']])

def refinement_dict_to_np(dict,size):
    ''' Convert refinement dict to numpy array 
        The refinement dict contains each timestep at wich new refinements where found
        The numpy array is the cumulated sum of nb of refinements found over all timesteps'''
    array = np.zeros(size)
    for k,v in dict.items():
        array[k] = v
    return np.cumsum(array)

def update_refinements_count(env_dbns,dbn,refinements_count,total_refinement_found,time_step):
    ''' take env_dbn and aglo dbn and return the total number of true refinements found
    '''
    total_valid_refinements,wrong = get_refinements_validity_count(dbn,env_dbns)
    new_refinements = total_valid_refinements-total_refinement_found
    if (new_refinements>0):
        refinements_count[time_step] = new_refinements
        print("refinement found")
    if wrong >0 :
        print('wrong found')
    return total_valid_refinements

def experiment(params):
    ''' Launch an expriment with params and save the results'''
        # init env : 
    if isinstance(params['env'],str) :
        env_name = params['env']
        env = gym.make(params['env'])
    else : 
        env_name = params['env'].__name__
        env = params['env']()
    #env.set_deterministic()
        # plot data : 
    envDBNs = env.getDBNs()
    refinement_counts = dict() # 
    total_refinements_found = 0
        # init agent :
    algo_params = {k:v for k,v in params.items() if k in ['K','Lambda','forget']}
    n_actions,n_state_var, state_var_dom, encoded_states,real_refinements_nb = list(ENV_INFOS[env_name].values())
    agent = Agent(n_actions, params['epsilon'], n_state_var, state_var_dom, algo_params)

    num_step = 0
    loop = True

    while loop: # loop for all episodes  
        # print(num_step)
        old_state = env.reset()
        done = False
        while not done: # end when episode completed
            if encoded_states:
                decoded_old_state = list(env.decode(old_state))
                action = agent.select_action(decoded_old_state)
                state, reward, done, info = env.step(action)
                # decode state to a (taxi_row, taxi_col, pass_pos, dest_idx) format
                decoded_state = list(env.decode(state))
                env_sample = (decoded_old_state, action, decoded_state, reward)
            else:
                action = agent.select_action(old_state)
                state, reward, done, info = env.step(action)
                env_sample = (old_state, action, state, reward)
            # add sample to the agent s dbn s buffer
            refinements = agent.update_dbn(env_sample)
            if refinements:
                total_refinements_found = update_refinements_count(envDBNs,agent.get_dbn(),
                                            refinement_counts,total_refinements_found,num_step)
            old_state = state
            # gather data for correct number of refinements made over time 
            num_step+=1
            if done : 
                old_state = env.reset()
                break
            if num_step >= params['nb_steps'] or total_refinements_found == real_refinements_nb: 
                loop = False
                break
        # creates folders : 
    date = datetime.datetime.now().strftime("%Y%m%d-%H%M%S") # use date for     
    run_folder = os.path.join(os.getcwd(),"runs")
    print(run_folder)
    param_folder = os.path.join(run_folder,make_str(params))
    date_folder = os.path.join(param_folder,date)
    for folder in [run_folder,param_folder,date_folder]:
        if not os.path.exists(folder):
            os.mkdir(folder)
        # save datas : 
    dbn = agent.get_dbn()
    dbn.save(os.path.join(date_folder,'dbn'))
    with open(os.path.join(param_folder,'params.txt'),"w") as f :
        params['env'] = env_name
        json.dump(params, f,indent=4)
    refinement_counts = refinement_dict_to_np(refinement_counts,params['nb_steps'])
    with open(os.path.join(date_folder,'refinements'),'wb') as f:
        np.save(f,refinement_counts)
    # plot data :
    # plt.plot(refinement_counts)
    # plt.show()
    # dbn.print()

def main():
    t = time.time()
    params = {
        'env' : CoffeeTask,
        'K' : 50*2,
        'Lambda' : 1,
        'epsilon' : 0.3,
        'DBNMetric' : 'BIC',
        'forget' : False,
        'nb_steps' : 200000,
        'K_Criterion' : 'pot_split'
    }
    experiment(params)
    t = time.time() - t
    print(">> Total time: ", t)

if __name__ == '__main__':
    for i in range(1):
        main()