import matplotlib.pyplot as plt
import os
import numpy as np
from statistics import mean
import seaborn as sns
import pickle

from DBN.dbn import get_refinements_validity_count
from DBN.cpt import to_digraph
from envs.coffee_task import CoffeeTask
sns.set()

def show_graph(path):
    ''' print cpt with dbn filepath , and a speficic action and state_var_id'''
    dbn = None
    with open(path, "rb") as input_file:
        dbn = pickle.load(input_file)    
    dbn.print()

def show_cpt(path,action,state_var_id):
    dbn = None
    with open(path, "rb") as input_file:
        dbn = pickle.load(input_file)    
    cpt = dbn.cpts[action][state_var_id]
    graph = to_digraph(cpt)
    graph.view()

def get_data_files(runs_dir,ignore = ['.DS']):
    '''
        return all file_path for a given run folder
        return dict of shape : 
            data_file['params']['refinements'] => [file_path,...]
            data_file['params']['dbn'] => [file_path,...] 
    '''
    run_groups = os.listdir(runs_dir)
    data_files = dict()
    for group in run_groups:
        data_files[group] = dict()
        refinements = []
        dbns = []
        if not ".DS" in group:
            runs = os.listdir(runs_dir + group)
            for run_folder in runs:
                if not ".DS" in run_folder and not ".txt" in run_folder:
                    #print(run_folder)
                    run_files = os.listdir(runs_dir + group + "/"+ run_folder)
                    for filename in run_files:
                        if "refinements" in filename: 
                            refinements.append(runs_dir + group + "/" + run_folder + "/refinements")
                        if "dbn" in filename :
                            dbns.append(runs_dir + group + "/" + run_folder + "/dbn")
        data_files[group]['refinements'] = refinements
        data_files[group]['dbn'] = dbns
    return data_files

def get_mean_wrong_refinements(runs_dir,real_dbn,verbose=False):
    ''' for each set of params returns the mean number of incorrect refinements'''
    data_files = get_data_files(runs_dir)
    for params in data_files:
        print("for params : ",params)
        wrong_refinements = np.zeros(len(data_files[params]['dbn']))
        for i,file in enumerate(data_files[params]['dbn']):
            with open(file, "rb") as f:
                dbn = pickle.load(f)
                _,wrong_refinements[i]  = get_refinements_validity_count(dbn,real_dbn,verbose=verbose)
        print('mean wrong refinements : ',np.mean(wrong_refinements))


def plot_run_group():
    # print(os.getcwd())
    run_groups = os.listdir("../runs/")

    for group in run_groups:
        curves_list = []
        if not ".DS" in group:
            runs = os.listdir("../runs/" + group)
            for run_folder in runs:
                if not ".DS" in run_folder and not ".txt" in run_folder:
                    # print(run_folder)
                    run_files = os.listdir("../runs/" + group + "/"+ run_folder)
                    for filename in run_files:
                        if "refinements" in filename: 
                            refinements = np.load("../runs/" + group + "/" + run_folder + "/refinements")
                            #print(refinements)
                            curves_list.append(refinements)
            # plot the average curve for each group of runs
            # max_nb_steps = max([len(curve) for curve in curves_list])

            #print(len(curves_list))
            
            avg_refinements = np.array([*map(mean, zip(*curves_list))])
            std_refinements = np.array([np.std(col) for col in zip(*curves_list)])
            
            forget = 'forget=True' in group
            if "epsilon=0.3" in group:
                plt.plot(avg_refinements, label = "Active BIC"+f"_forget={forget}")
            elif "epsilon=1" in group:
                plt.plot(avg_refinements, label = "Passive BIC"+f"_forget={forget}")
            
            plt.fill_between([i for i in range(200000)] ,avg_refinements-std_refinements, avg_refinements+std_refinements, alpha=0.25, linewidth=0)
        
    plt.ylabel('Average correct refinements')
    plt.xlabel('Time steps')
    plt.title("Results in the coffee task")
    plt.legend()
    plt.show()
            

## test
if __name__ == '__main__':
    plot_run_group()
    get_mean_wrong_refinements('../runs/',CoffeeTask().getDBNs(),verbose=False)
