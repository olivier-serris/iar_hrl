from DBN.node import Split_Node
import random as rd
from DBN.dbn import DBN
from DBN.cpt import CPT
import numpy as np

class Agent:
    '''
    Agent that interacts with the environmenent, choosing actions in order to maximize entropy.
    '''

    def __init__(self, n_actions, epsilon, n_state_var, state_var_dom, param):
        '''
        Agent initialization
        @param n_actions: number of possible actions
        '''
        self.n_actions = n_actions
        self.epsilon = epsilon
        self.dbn = DBN(n_state_var, n_actions, state_var_dom, param)

    def select_action(self, state):
        '''
        epsilon constant?
        @param state: current state of the environment
        '''

        if rd.random() < self.epsilon:
            # return a random action
            action = rd.randint(0, self.n_actions-1)
        else:
            action, entropy_change = self.dbn.get_best_action(state)

            if entropy_change <= 0:
                action = rd.randint(0, self.n_actions-1)

        return action

    def update_dbn(self, env_sample):
        '''
        Updates the internal DBN of the agent given the new data sample from the environment.
        @env_sample: data sampled from the environment
        '''
        refinements_found = self.dbn.update(env_sample)
        return refinements_found
    
    def get_dbn(self):
        return self.dbn

        

