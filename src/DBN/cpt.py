import numpy as np
from typing import Tuple
from DBN.node import Node, Leaf, Split_Node
from graphviz import Digraph


def potential_parent_ll(hists :np.array):
    '''log likelyhood on potential parents histogram matrix '''
    probas = hists/ np.sum(hists,axis=1)[:,None]
    probas = np.where(hists==0,1,probas) #  if no data replace value of proba to avoid nan in log
    ll = np.multiply(hists, np.log(probas))
    return np.sum(ll)

class CPT :
    ''' Conditionnal probability tree for a specific variable of the state and for a specific action '''
    def __init__(self,action,state_var,state_var_dom,n_state_var):
        ''' 
        Each CPT is associated with a pair : <state_var,action>
        state_var_dom : the domaine of a variable of the state
        n_state_var : number of variable in the state
        '''
        self.state_var = state_var 
        self.root = Leaf(state_var_dom,list(range(n_state_var)),state_var)
        self.action = action

    def update(self,buffer,data_id,K,Lambda,data_len,forget):
        '''Update CPT with new data instance.''' 
        refinement = None
        old_state,action,state,reward = buffer[data_id]
        # Find and update leaf associated with the data instance
        associated_leaf,parent_node,state_var_val = self.find_associated_leaf(old_state)
        associated_leaf.add_instance(data_id,buffer)
        # find buffer len : 
        if forget :
            buffer_len = np.sum(data_len[action,:])# |D| = Sum_{i,j,k} N_{i,j,k}  
        else : 
            buffer_len = data_len[action] * len(state)# |D| = Nb data stored for actiion a 

        # if enough samples, find best refinement for this leaf 
        if associated_leaf.enough_sample(K): 
            if action == 0 and self.state_var == 3 and parent_node!=None and state_var_val!=1: 
                test = None
            split_var_id = self.find_best_refinement(associated_leaf,Lambda,buffer_len)
            if forget :
                    data_len[action,self.state_var] -= len(associated_leaf.data)
                    assert(data_len[action,self.state_var]>=0)
                    associated_leaf.reset_data()
            # if new refinement found, add it to the CPT : 
            if split_var_id is not None : 
                refinement = associated_leaf.split(split_var_id,buffer)
                if parent_node == None : 
                    self.root = refinement
                else :
                    parent_node.childrens[state_var_val] = refinement
        return refinement

    def find_associated_leaf(self,parent_state) -> Tuple[Leaf,Node,int]:
        ''' Map data instance to associated leaf : (find the leaf for wich the value of the old state matches the preconditions of the leaf)
            Meaning that the parent state values  matches the precondition of the leave 
            returns a tuple containing : 
            <the leaf, the parent node or None, the value of the split variable for the parent or None>'''
        subnode = self.root
        parent_node = None
        split_var_val = None
        while isinstance(subnode,Split_Node):
            var_val = parent_state[subnode.split_var]
            parent_node = subnode
            split_var_val = var_val
            subnode = subnode.childrens[var_val]
        return subnode,parent_node,split_var_val

    def find_best_refinement(self,associated_leaf : Leaf,Lambda,buffer_len) -> Split_Node:
        ''' Construct all possibles refinements
            Compute BIC score associated with each refinement 
            return split_var_id with highest BIC score or None if the original leaf has a better score'''
        assert(buffer_len>0)
        scores = []
        # test scores of each potential split nodes : 
        for split_var_id in associated_leaf.potentials_hists.keys():
            p_hists = associated_leaf.potentials_hists[split_var_id]
            ll= potential_parent_ll(p_hists)
            reg =  p_hists.shape[0]*associated_leaf.state_var_dom *0.5 *np.log(buffer_len)
            BIC = ll - Lambda * reg
            scores.append((BIC,split_var_id))
        if scores : 
            # keep only the best refinement
            best_bic,best_split = max(scores,key=lambda x : x[0])
            original_leaf_score = associated_leaf.get_BIC_score(Lambda,buffer_len)
            # check if the best refinement has a better score then the leaf alone
            if best_bic > original_leaf_score:
                # New refinement found 
                return best_split
        return None


    def __repr__(self) -> str:
        return f'CPT<a={self.action},s_i={self.state_var}> :'

    def clean_before_save(self):
        self.root.clean_before_save()

########## utils : 

def to_digraph(cpt,state_label=None,action_label=None):
    ''' Convert CPT structure to graphiz Digraph class'''
    def add_node(graph,node,parent_name=None,split_val=None):
        if isinstance(node,Leaf):
            node_name = f'leaf_{node.proba}'#str(node)
            graph.node(node_name)
        elif isinstance(node,Split_Node):
            node_name = state_label[cpt.split_var] if state_label else str(node.split_var)
            graph.node(node_name)
            for split_val,c in node.childrens.items():
                add_node(graph,c,parent_name=node_name,split_val=split_val)
        if parent_name : 
            graph.edge(parent_name,node_name , label=str(split_val))            
    action_name = action_label if action_label else cpt.action
    graph = Digraph(f'CPT_{action_name},{cpt.state_var}')
    add_node(graph,cpt.root,None)
    return graph
