
class Buffer:
    ''' Contains all data instance with shape : 
        (old_state,action,cur_state,reward)
        '''
    def __init__(self):
        self.data = []

    def add(self,instance):
        ''' Add data instance to buffer 
            return id of data instance in buffer''' 
        self.data.append(instance)
        return len(self)-1
    
    def __getitem__(self, key):
        return self.data[key]

    def __len__(self):
        return len(self.data)