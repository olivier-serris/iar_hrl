from node import Leaf
from buffer import Buffer
from cpt import potential_parent_ll
import numpy as np 

def ll_test():
    ''' Verify loglikelihood'''
    hists = np.array([[0,2],
            [0,4]])
    assert(potential_parent_ll(hists) == 0)

    hists = np.array([[0,2],
            [4,4]])
    assert potential_parent_ll(hists) == 2 * np.log(1) +  8 * np.log(1/2)
    hists = np.array([[1,3],
            [1,4]])
    assert(potential_parent_ll(hists) == 1 * np.log(1/4) + 3 * np.log(3/4) 
                                       + 1 * np.log(1/5) + 4 * np.log(4/5))



def entropy_test():
    state_var_dom = 2
    pot_parents = [0]
    cpt_var_id = 0
    leaf = Leaf(state_var_dom,pot_parents,cpt_var_id)
    buffer = Buffer()
    data_ids =  [ ([0],0,[0],None),
                  ([1],0,[0],None)]
    for d in data_ids:
        buffer.add(d)
    
    leaf.add_instance(0,buffer)
    entropy_change = leaf.get_entropy_change(data_ids[1])
    assert(entropy_change == 1)

def leaf_tests():
    entropy_test()
    ll_test()


def main():
    leaf_tests()

if __name__ == '__main__':
    main()
