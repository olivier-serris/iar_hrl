
from DBN.buffer import Buffer
from DBN.cpt import CPT
import numpy as np
import copy 
import pickle
from DBN.node import get_all_split
#TODO : 
    # In current code all states variables must have same domain 
    #-->  change to allow each state variable to have it's own domain.

class DBN :
    ''' 
    Dynamique Bayes Network 
    Contains the set of CPT for each pair <a,state_var>
    '''
    def __init__(self,n_states_var,n_actions,state_var_dom,param):
        ''' 
        n_state_var : the number of variables in the state
        n_actions : the number of possible actions
        state_var_dom : the domaine of any variable of the state (we supposed they all have the same domain)
        param : dictionnary containing hyper-parameters of the algorithm : 
            K : minimum number of data-instance in a leaf before performing split
            lambda: regularisation parameter for the BIC score
        '''
        self.cpts = dict()
        self.buffer = Buffer()
        self.n_actions = n_actions
        self.n_states_var = n_states_var
        self.param = param 

        if self.param['forget']:
            # store data len for each CPT
            self.data_lens = np.zeros((n_actions,n_states_var))
        else :
            # store len for each actions
            self.data_lens = np.zeros(n_actions)
        for a in range(n_actions):
            self.cpts[a] = dict()
            for s in range(n_states_var):
                self.cpts[a][s] = CPT(a,s,state_var_dom,n_states_var)

    def get_best_action(self,state) -> int:
        ''' return the action that maximises the entropy metric and the corresponding entropy change value'''
        entropy_score = np.zeros(self.n_actions) 
        for action in range(self.n_actions):
            for state_var in range(self.n_states_var):
                cpt = self.cpts[action][state_var]
                associated_leaf, parent_node, split_var_val = cpt.find_associated_leaf(state)
                entropy_change = associated_leaf.get_entropy_change(state)
                entropy_score[action] += entropy_change
        best_action = np.argmax(entropy_score)
        max_entropy_change = np.max(entropy_score)
        return best_action, max_entropy_change

    def update(self,data_instance):
        '''
        Update the DBN with new data_instance 
        Add the data instance to the corresponding CPT
        Check if refinements can be made
        
        data_instance : tuple of <s,a,s'>
        '''
        refinements_founds = []
        data_id = self.buffer.add(data_instance)
        s_old,a,s,r = data_instance
        self.data_lens[a] +=1 # count data for bic score
        for cpt in self.cpts[a].values():
            split_node = cpt.update(self.buffer,data_id,self.param['K'],
                                    self.param['Lambda'],self.data_lens,self.param['forget'])
            if split_node != None : 
                refinements_founds.append((cpt,split_node))
        return refinements_founds

    def print(self):
        ''' print the tree for debug purpose '''
        for action in self.cpts :
             for cpt in self.cpts[action].values():
                 print(cpt)
                 pprint_tree(cpt.root)

    def save(self,filename):
        ''' Save th dbn structure ''' 
        to_save = copy.deepcopy(self)
        # clean leaves from un-necessary data : 
        for a in range(self.n_actions):
            for s in range(self.n_states_var):
                to_save.cpts[a][s].clean_before_save()
        with open(filename,'wb') as f: 
            pickle.dump(to_save,f)
    

############ utils functions : 
        
def pprint_tree(node, file=None, _prefix="", _last=True):
    ''' print for debug purpose'''
    # TODO : find a more readable presentation to display the data. 
    print(_prefix, "`- " if _last else "|- ", str(node), sep="", file=file)
    _prefix += "   " if _last else "|  "
    child_count = len(node.childrens)
    for i, child in node.childrens.items():
        _last = i == (child_count - 1)
        pprint_tree(child, file, _prefix, _last)


def get_refinements_validity_count(dbn,real_dbn,verbose = False):
    '''Returns a tuple (nb of valid refinements ,nb of invalid refinements) '''
    true_refinements = 0
    false_refinements = 0
    for a in range(dbn.n_actions):
        for s in range(dbn.n_states_var):
            cpt = dbn.cpts[a][s]
            split_nodes = get_all_split(cpt.root)
            split_vars = {split.split_var for split in split_nodes}
            real_splits = set(real_dbn[a][s] if s in real_dbn[a] else [])
            valid = len(real_splits.intersection(split_vars))
            # special case : a refinement on self var is always correct (but not counted as such)
            real_splits.add(s) 
            invalid = len(split_vars.difference(real_splits))
            if verbose :
                for split in split_vars.difference(real_splits):
                    print(f"incorrect : action={a},state={s},split={split}")
            true_refinements+=valid
            false_refinements+=invalid
    return true_refinements,false_refinements



