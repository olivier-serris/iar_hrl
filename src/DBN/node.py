
import numpy as np
from scipy.stats import entropy
from typing import Dict

class Node:
    ''' Node in the CPT '''
    def __init__(self):
        self.childrens = dict()
    def add_instance(self):
        raise NotImplementedError()
    def clean_before_save(self):
        for c in self.childrens.values() :
            c.clean_before_save()

class Split_Node(Node):
    ''' Node that indicate a split for a specific var in the CPT 
        Contains a dictionnary of children  : 
        {Key = value of split variable : Value = Node }
        Children nodes can be other Split_Node or Leafs.
    '''
    def __init__(self,split_var,childrens :Dict[int,Node]):
        '''
        split_var : the id of the variable on wich the split is performed
        childrens : dictionnary {value of split variable : Node}
        '''
        super(Split_Node, self).__init__()
        self.split_var = split_var
        self.childrens = childrens

    def get_BIC_score(self,Lambda,buffer_len) -> int:
        ''' Returns the BIC score of th subtree 
            Lambda : regularisation parameters
            buffer_len : number of total data instance collected. 
        '''
        return sum(children.get_BIC_score(Lambda,buffer_len) for children in self.childrens.values())
    
    def add_instance(self,data_id,buffer):
        old_state,action,state,reward = buffer[data_id]
        parent_val = old_state[self.split_var]
        self.childrens[parent_val].add_instance(data_id,buffer)

    def __repr__(self) -> str:
        return f'SplitNode Var{self.split_var}'

class Leaf(Node): 
    ''' Leaf of the CPT for a variable : 
        Contains the conditionnal probability of the value of the variable.
        A leaf contains the following variables : 
            @data : list of all the data instance that map to that leaf
            @histogram : histogram for the variable of the CPT
            @potentials_hists : a dictionnary.
                each key is the id of a state variable on wich we can split the leave
                each value is a 2D matrix of shape :  dom(split_var),dom(cpt_var)
                    the line number represent one of the value of the split_var
                    each line is an histogram over the values of the CPT var. 
    '''
    def __init__ (self,state_var_dom,potential_parents,cpt_var_id):
        '''
        cpt_var_id : indicates the id of the variable for wich we can compute conditionnal probability
        state_var_dom : the domaine of one variable of the state.
        potential_parents : list of id of state variable on wich a split can be made (variables that are not on the path from the leaf to the root)
        '''
        super(Leaf, self).__init__()
        self.state_var_dom = state_var_dom
        self.data = []
        self.histogram = np.zeros(state_var_dom)
        self.cpt_var_id = cpt_var_id

        self.potentials_hists = {p : np.zeros((state_var_dom,state_var_dom)) for p in potential_parents}
    
    @property
    def proba(self):
        '''return conditionnal probability of the value of the variable'''
        return self.histogram / np.sum(self.histogram)
        
    @property
    def is_empty(self):
        ''' return True if no data-instance have been mapped to the leaf (=no data instance matches the precondition of the path from leaf to root)'''
        return sum(self.histogram) == 0
    
    @property
    def potential_parents(self):
        ''' return potential_parents : list of id of state variable on wich a split can be made.
        (variables that are not on the path from the leaf to the root)'''
        return self.potentials_hists.keys()
        
    def get_LL_score(self) -> int:
        ''' Returns the LogLikelyhood score associated with a leaf'''
        return sum(np.log(p)*n for p,n in zip(self.proba,self.histogram) if n >0)

    def get_BIC_score(self,Lambda,buffer_len)->int :
        ''' Returns the BIC score of the leaf 
            Lambda : regularisation parameters
            buffer_len : number of total data instance collected. 
        '''
        if self.is_empty: 
            return 0
        regularisation = Lambda * (self.state_var_dom/2) * np.log(buffer_len)
        return self.get_LL_score() - regularisation

    def add_instance(self,data_id,buffer):
        ''' Adds an instance to a Leaf : 
            update data-instances and update histogram.'''
        old_state,action,state,reward = buffer[data_id]
        self.data.append(data_id)
        self.histogram[state[self.cpt_var_id]] +=1
        for par_id in self.potential_parents: 
            parent_var_val = old_state[par_id]
            self.potentials_hists[par_id][parent_var_val][state[self.cpt_var_id]]+=1

    def split(self,split_var_id,buffer)->Split_Node :
        '''
        split_var_id : the variable of the state on wich the split is performed
        return a split_node
        '''
        leaf_pot_parents = list(filter(lambda p: p!= split_var_id, self.potential_parents))

        childrens = {split_val : Leaf(self.state_var_dom,leaf_pot_parents,self.cpt_var_id) 
                                                for split_val in range(self.state_var_dom)}
        split_node = Split_Node(split_var_id,childrens)
        for id in self.data:
            split_node.add_instance(id,buffer)
        return split_node

    def get_entropy_change(self,parent_state) -> int:
        ''' 
            For each vector M of a potential parent compute the change in entropy of adding a new data instance.
            return the entropy change summed over all potential parents. 
        '''
        s_old = parent_state
        entropy_change = 0
        for parent in self.potential_parents:
            parent_val = s_old[parent]
            M = np.sum(self.potentials_hists[parent],axis=1) # Vector M as describe in the article
            if np.sum(M)==0 :
                old_entropy = 0
            else : 
                old_entropy = entropy(M,base=2)
            M[parent_val]+=1
            new_entropy = entropy(M,base=2)
            entropy_change +=  new_entropy - old_entropy 
        assert entropy_change==entropy_change
        return entropy_change

    def enough_sample(self,K) -> bool:
        '''check if leaf has enough samples to compute refinement 
            K : threshold '''
        # TODO : check if it is the good criterion in the article
        # Checks if each non empty leaves maps 
        at_leat_K = True 
        for p in self.potentials_hists : 
            parent_hist = np.sum(self.potentials_hists[p],axis=1)
            if np.logical_and(parent_hist != 0, parent_hist < K).any() :
                at_leat_K = False
        return at_leat_K

    def clean_before_save(self):
        ''' Remove data we don't we to save'''
        self.data = None
        self.potentials_hists = None
    def reset_data(self):
        ''' Remove all data in Leaf'''
        self.data = []
        self.histogram = np.zeros(self.state_var_dom)
        self.potentials_hists = {p : np.zeros((self.state_var_dom,self.state_var_dom)) for p in self.potential_parents}

    def __repr__(self) -> str:
        if self.is_empty : 
            return 'Leaf empty'
        return f'Leaf : {self.proba}'

########### Utils function ###########

def get_all_split(node):
    ''' returns the set of all childs split_node of node'''
    splits = set()
    if isinstance(node,Split_Node):
        splits.add(node)
    for children in node.childrens.values():
        splits = splits.union(get_all_split(children)) 
    return splits