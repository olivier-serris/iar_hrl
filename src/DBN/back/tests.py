from dbn import DBN
from node import *
from agent import Agent
import gym
import time

def test_OneVariable():
    fake_samples = [
    # state params   (S, a, S', R  )
                    ([0],0,[0],None),
                    ([0],0,[1],None),
                    ([1],0,[1],None)
                    ]
    n_actions = 1
    n_state_var = 1
    state_var_dom = 2
    # algo params : 
    param = {'K' : 10 , 'lambda' : 1}
    dbn = DBN(n_state_var,n_actions,state_var_dom,param)
    for epoch in range(1):
    # for epoch in range(1000): 
        for s in fake_samples: 
            dbn.update(s)
    dbn.print()

def test_MultipleVariable():
    
    # Ce test ne donne pas le meilleur BDN possible 
    # Mais c'est probalement normal conte tenu du fait qu'il est construit de façon itérative.
    
    fake_samples = [
    # state params  ( S   ,a, S'  , R  )
                    ([0,0],0,[0,0],None),
                    ([0,1],0,[0,1],None),
                    ([1,0],0,[0,1],None),
                    ([1,1],0,[0,0],None),

                    ([0,0],1,[0,0],None),
                    ([0,1],1,[0,1],None),
                    ]
    n_actions = 2
    n_state_var = 2
    state_var_dom = 2
    # algo params : 
    param = {'K' : 10 , 'lambda' : 1}
    dbn = DBN(n_state_var,n_actions,state_var_dom,param)
    for epoch in range(1000): 
        for s in fake_samples: 
            dbn.update(s)
    dbn.print()

def test_buffer():
    buff = Buffer()
    fake_samples = [
    # state params   (S, a, S', R  )
                    ([0],0,[0],None),
                    ([0],0,[1],None),
                    ([1],0,[1],None)
                    ]
    for data_instance in fake_samples:
        buff.add(data_instance)
    print(buff)

def test_leaf():
    state_var_dom = 2
    potential_parents = [0,1,2,3,4]
    cpt_var_id = 4
    leaf = Leaf(state_var_dom,potential_parents,cpt_var_id)
    data_id = 2
    fake_samples = [
    # state params   (S, a, S', R  )
                    ([0],0,[0],None),
                    ([0],0,[1],None),
                    ([1],0,[1],None)
                    ]

    leaf.add_instance(data_id, fake_samples)
    print(leaf)

def test_env():
    # params about the taxi env
    n_actions = 6
    n_state_var = 4
    state_var_dom = 5

    # params about the algorithm
    algo_params = {'K' : 50*5 , 'lambda' : 1}
    epsilon = 0.6

    # environment creation
    env = gym.make('Taxi-v3')
    old_state = env.reset()

    # agent creation
    agent = Agent(n_actions, epsilon, n_state_var, state_var_dom, algo_params)

    t = time.time()
    for episode in range(10000):
        print("Episode ", episode)
        # decode state to a (taxi_row, taxi_col, pass_pos, dest_idx) format
        decoded_old_state = list(env.decode(old_state))

        action = agent.select_action(decoded_old_state)
        state, reward, done, info = env.step(action)
        
        # decode state to a (taxi_row, taxi_col, pass_pos, dest_idx) format
        decoded_state = list(env.decode(state))

        # add sample to the agent s dbn s buffer
        env_sample = (decoded_old_state, action, decoded_state, reward)
        agent.update_dbn(env_sample)
        old_state = state
    
    dbn = agent.get_dbn()
    dbn.print()
    t = time.time() - t
    print(">> Total time: ", t)
    # avg_rewards, best_avg_reward = interact(env, agent)

def main():
    #test_OneVariable()
    #print("_________")
    #test_MultipleVariable()


    # state_var_dom = 2
    # potential_parents = [0,1,2,3,4]
    # cpt_var_id = 4

    # buff = Buffer()
    # 
    # fake_samples = [
    # # state params   (S, a, S', R  )
    #                 ([0],0,[0],None),
    #                 ([0],0,[1],None),
    #                 ([1],0,[1],None)
    #                 ]

    # data_id = 2


    test_env()
    

if __name__ == '__main__':
    main()