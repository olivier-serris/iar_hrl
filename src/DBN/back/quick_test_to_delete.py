from dbn import DBN
def test_OneVariable():
    fake_samples = [
    # state params   (S, a, S', R  )
                    ([0],0,[0],None),
                    ([0],0,[1],None),
                    ([1],0,[1],None)
                    ]
    n_actions = 1
    n_state_var = 1
    state_var_dom = 2
    # algo params : 
    param = {'K' : 10 , 'lambda' : 1}
    dbn = DBN(n_state_var,n_actions,state_var_dom,param)
    for epoch in range(1000): 
        for s in fake_samples: 
            dbn.update(s)
    dbn.print()

def test_MultipleVariable():
    
    # Ce test ne donne pas le meilleur BDN possible 
    # Mais c'est probalement normal conte tenu du fait qu'il est construit de façon itérative.
    
    fake_samples = [
    # state params  ( S   ,a, S'  , R  )
                    ([0,0],0,[0,0],None),
                    ([0,1],0,[0,1],None),
                    ([1,0],0,[0,1],None),
                    ([1,1],0,[0,0],None),

                    ([0,0],1,[0,0],None),
                    ([0,1],1,[0,1],None),
                    ]
    n_actions = 2
    n_state_var = 2
    state_var_dom = 2
    # algo params : 
    param = {'K' : 10 , 'lambda' : 1}
    dbn = DBN(n_state_var,n_actions,state_var_dom,param)
    for epoch in range(100): 
        for s in fake_samples: 
            dbn.update(s)
    dbn.print()

def main():
    test_OneVariable()
    print("_________")
    test_MultipleVariable()
    

if __name__ == '__main__':
    main()