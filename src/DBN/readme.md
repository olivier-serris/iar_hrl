# Dynamique Bayes Network (DBN) for reinforcement learning

Implementation of article : <br>
Active Learning of Dynamic Bayesian Networks in Markov Decision Processes [2007] Anders Jonsson and Andrew Barto


Algorithm : 

### Simplification du calcul de score BIC : 

- le score de BIC pour un DBN et un ensemble de données : <br>
<img src="https://render.githubusercontent.com/render/math?math=BIC(D,G) = [\sum_i \sum_j \sum_k N_{i,j,k} log \theta_{i,j,k}] - \frac{|\theta|}{2}log(|D|)"><br>
<img src="https://render.githubusercontent.com/render/math?math=|\theta| = \sum_i \sum_j\sum_kdom(X_i))">
<img src="https://render.githubusercontent.com/render/math?math=BIC(D,G) = \sum_i \sum_j \sum_k N_{i,j,k} log \theta_{i,j,k} - \frac{dom(X_i))}{2}log(|D|)"><br>
- On peut séparer le calcul pour chaque variables : <br>
<img src="https://render.githubusercontent.com/render/math?math=BIC(D,G) = \sum_i BIC(D,G,i)"><br>
<img src="https://render.githubusercontent.com/render/math?math=BIC(D,G,i) =[\sum_j \sum_k N_{i,j,k} log \theta_{i,j,k} - \frac{|Dom(X_i)|}{2}log(|D|) ]"><br>
- On peut séparer le calcul pour chaques feuilles : <br>
<img src="https://render.githubusercontent.com/render/math?math=BIC(D,G,i,j) =[\sum_k N_{i,j,k} log \theta_{i,j,k} - \frac{|Dom(X_i)|}{2}log(|D|) ]">
